package homework2

fun main() {
    val limonad: Int = 18500
    val pinaKolada: Short = 200
    val viski: Byte = 50
    val fresh = 3000000000L
    val cola = 0.5f
    val ell: Double = 0.666666667
    val sekretniNapitok: String = "\"Что-то авторское\""
    println("Заказ - '$limonad мл лимонада' готов")
    println("Заказ - '$pinaKolada мл пина колады' готов")
    println("Заказ - '$viski мл виски' готов")
    println("Заказ - '$fresh капель фреша' готов")
    println("Заказ - '$cola литра колы' готов")
    println("Заказ - '$ell литра эля' готов")
    println("Заказ - '$sekretniNapitok' готов")
}