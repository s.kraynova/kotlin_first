package homework2

fun main() {
    val limonad: Int = 18500
    val pinaKolada: Short = 200
    val viski: Byte = 50
    val fresh = 3000000000L
    val cola = 0.5f
    val ell: Double = 0.666666667
    val sekretniNapitok: String = "\"Что-то авторское\""
    println("Илья выполнил заказ и разлил напитки следующим образом: \n $limonad мл лимонада налил в емкость Int. \n $pinaKolada мл пина колады налил в емкость Short. \n $viski мл виски налил в Byte. \n $fresh капель фреша налил в Long. \n $cola л колы налил в Float. \n $ell л эля налил в Double. \n Приготовил $sekretniNapitok в String. \n И пожелал приятного вечера клиентам. =)" )
}