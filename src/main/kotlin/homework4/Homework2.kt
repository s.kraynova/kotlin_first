package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var otlichniki = 0
    var horohisty = 0
    var troechniki = 0
    var dvoechniki = 0

for (mark in marks) {
    when (mark) {
        5 -> otlichniki++
        4 -> horohisty++
        3 -> troechniki++
        2 -> dvoechniki++
    }
}
    val persent5 = ((otlichniki.toDouble()/(marks.size))*100)
    println("Отличников - $persent5%")

    val persent4 = ((horohisty.toDouble()/(marks.size))*100)
    println("Хорошистов - $persent4%")

    val persent3 = ((troechniki.toDouble()/(marks.size))*100)
    println("Троечников - $persent3%")

    val persent2 = ((dvoechniki.toDouble()/(marks.size))*100)
    println("Двоечников - $persent2%")
    }
