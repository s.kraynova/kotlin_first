package homework7

fun main() {
    println("Введите логин и нажмите Enter. Введите пароль и нажмите Enter.")
    registerUser(User(login=readLine().toString(), password =readLine().toString()))
}

fun registerUser( myUser: User) {
    if (myUser.login.count() > 20) { throw WrongLoginException() }
    if (myUser.password.count() < 10) {throw WrongPasswordException("В пароле не должно быть менее 10 символов. Начните заново")
    }
    println("Подтвердите пароль")
    val confirmPassword: String = readLine().toString()
    if (myUser.password != confirmPassword) {throw WrongPasswordException("Пароль и подтверждение пароля должны совпадать. Начните заново") }
    else {println("Регистрация завершена") }

}

class User(var login: String, var password: String) {
}

class WrongLoginException: Exception("В логине не должно быть более 20 символов. Начните заново") {
}
class WrongPasswordException(override val message: String) : Exception() {
}

