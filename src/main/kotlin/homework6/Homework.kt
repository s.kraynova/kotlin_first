package homework6


fun main() {
    val myPetArray = arrayOf(
        Wolf("Bonk", 1, 27),
        Lion("Simba", 1, 34),
        Behemot("Glory", 2, 89),
        Tiger("Ring", 2, 44),
        Jirafe("Rafik", 5, 56),
        Slon("Elle", 4, 98),
        Shimpanze("Mark", 1, 29),
        Gorilla("Rock", 1, 45)
    )
    println("Какой едой вы кормите питомцев? Перечислите продукты через пробел, если их больше одного.")
    val arrayProduct: List<String> = readLine()!!.split(" ")
    startMassEat(myPetArray, arrayProduct)
}

fun startMassEat(arrayAnimals: Array<Animal>, arrayProduct: List<String>) {
    for (animal in arrayAnimals) {
        for (product in arrayProduct) {
            animal.eat(product)
        }
    }
}

abstract class Animal( open val name: String, open val growth: Int, open val weight: Int) {
   private var satiety = 0
    abstract var  predpochteniyaEda: Array<String>
   internal fun eat(product: String) {
if (product in predpochteniyaEda) {
               satiety++
               println("Питомцу по имени $name понравилась еда $product. Текущий показатель сытости $satiety")
           }
    else println("Питомцу по имени $name не понравилась еда $product. Текущий показатель сытости $satiety")
   }
}

class Wolf( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("meat")
}
class Lion( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("meat", "fish")
}
class Behemot( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("ovoshi", "trava")
}
class Tiger( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("meat", "fish")
}
class Jirafe( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("ovoshi", "trava", "listya", "salat", "vetochki")
}
class Slon( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("ovoshi", "trava", "listya", "salat")
}
class Shimpanze( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("ovoshi", "salat", "banana")
}
class Gorilla( override val name: String, override val growth: Int, override val weight: Int): Animal(name, growth, weight) {
    override var predpochteniyaEda = arrayOf("salat", "banana")
}