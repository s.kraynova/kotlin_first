package homework5

fun main() {
    println("Введите число для переворачивания")
    var digit = readLine()!!.toInt()
    var reverce = 0
    while ( digit != 0) {
        val vspomogatelnoe = digit % 10
        reverce = reverce * 10 + vspomogatelnoe
        digit /= 10
    }
    println("Получилось перевернутое число $reverce")

}

//write your function here