package homework5

fun main() {
    val myPet1 = Wolf("Bonk", 1, 27)
    val myPet2 = Lion("Simba", 1, 34)
    val myPet3 = Behemot("Glory", 2, 89)
    val myPet4 = Tiger("Ring", 2, 44)
    val myPet5 = Jirafe("Rafik", 5, 56)
    val myPet6 = Slon("Elle", 4, 98)
    val myPet7 = Shimpanze("Mark", 1, 29)
    val myPet8 = Gorilla("Rock", 1, 45)
    myPet1.eat()
    myPet2.eat()
    myPet3.eat()
    myPet4.eat()
    myPet5.eat()
    myPet6.eat()
    myPet7.eat()
    myPet8.eat()
}

class Wolf( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("meat")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
     this.satiety = satiety
     this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        val product = readLine().toString()
        if (product in predpochteniyaEda ) {
                satiety++
                println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
            }
           else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
        }
}

class Lion( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("meat", "fish")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
        this.satiety = satiety
        this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        var product = readLine().toString()
        if (product in predpochteniyaEda ) {
            satiety++
            println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
        }
        else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
    }
}
class Behemot( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("ovoshi", "trava")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
        this.satiety = satiety
        this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        var product = readLine().toString()
        if (product in predpochteniyaEda ) {
            satiety++
            println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
        }
        else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
    }
}
class Tiger( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("meat", "fish")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
        this.satiety = satiety
        this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        var product = readLine().toString()
        if (product in predpochteniyaEda ) {
            satiety++
            println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
        }
        else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
    }
}
class Jirafe( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("ovoshi", "trava", "listya", "salat", "vetochki")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
        this.satiety = satiety
        this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        var product = readLine().toString()
        if (product in predpochteniyaEda ) {
            satiety++
            println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
        }
        else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
    }
}
class Slon( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("ovoshi", "trava", "listya", "salat")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
        this.satiety = satiety
        this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        var product = readLine().toString()
        if (product in predpochteniyaEda ) {
            satiety++
            println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
        }
        else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
    }
}
class Shimpanze( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("ovoshi", "salat", "banana")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
        this.satiety = satiety
        this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        var product = readLine().toString()
        if (product in predpochteniyaEda ) {
            satiety++
            println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
        }
        else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
    }
}
class Gorilla( var name: String, var growth: Int, var weight: Int ) {
    var  satiety = 0
    var predpochteniyaEda = arrayOf("salat", "banana")
    constructor(name: String, growth: Int,  weight: Int, satiety: Int, predpochteniyaEda: Array<String> ): this (name, growth, weight) {
        this.satiety = satiety
        this.predpochteniyaEda = predpochteniyaEda
    }
    fun eat() {
        println("Вы кормите питомца по имени $name. Какой продукт вы предлагаете ему?")
        var product = readLine().toString()
        if (product in predpochteniyaEda ) {
            satiety++
            println("Питомцу по имени $name понравилась еда. Текущий показатель сытости $satiety")
        }
        else println("Питомцу по имени $name не понравилась еда. Сытость не повысилась")
    }
}