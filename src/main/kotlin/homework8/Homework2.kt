package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    podschetOvoshei()
    podschetfinalCost()
}

fun podschetOvoshei() {
var countOfVegi = 0
    for ((key, value) in userCart) {
        for (item in vegetableSet) {
            if (item == key) {
                countOfVegi += value
            }
        }
    }
    println("Количество овощей в корзине: $countOfVegi")
}

fun podschetfinalCost() {

    var finalCost: Double = 0.0

    for ((key, value) in userCart) {
        if (key in discountSet) {
                finalCost += prices.getValue(key) * (1 - discountValue) * userCart.getValue(key)
        }
        else finalCost += prices.getValue(key) * userCart.getValue(key)
    }

    println("Итоговая цена овощей в корзине: $finalCost")
}
